class CreateComentarios < ActiveRecord::Migration
  def change
    create_table :comentarios do |t|
      t.string :autor
      t.text :cuerpo
      t.date :fecha

      t.timestamps null: false
    end
  end
end
